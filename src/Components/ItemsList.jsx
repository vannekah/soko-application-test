import React from 'react'
import itemDetails from '../util/itemDetails'
import ProductCard from './ProductCard'
import Form from './Form'
import SectionTitle from './SectionTitle'

function ItemsList() {
    return (
        <div className="itemsContainer" id="appItem">
            <Form />
            <div>
                <SectionTitle
                    titleName='Electronics'
                    numberOfItems='12'
                />
            </div>
            <div className="productCardContainer">
            {
                itemDetails.details.map((eachItem, index) => {
                    return (
                        <ProductCard key={index} itemDetails={eachItem} />
                    )
                })
            }
            </div>
        </div>
    )
}

export default ItemsList
