import React from 'react'

function Footer() {
    return (
        <div className="footerContainer" >
            <ul className="companyFeatures">
                <li>
                    <img src="./images/delivery_icon.png" alt="" />
                    <p>Fast delivery</p>
                </li>
                <li>
                    <img src="./images/guarantee_icon.png" alt="" />
                    <p>Buyer Protection</p>
                </li>
                <li>
                    <img src="./images/twentyfour_icon.png" alt="" />
                    <p>Customer Support</p>
                </li>
            </ul>
            <div className="companyDetails">
                <p className="storeDetails">store details</p>
                <p className='companyName'>Target</p>
                <p className='companyAddress'>Cham Towers, Plot 12 Nkruma Rd, Kampala, UG</p>
            </div>
        </div>
    )
}

export default Footer
